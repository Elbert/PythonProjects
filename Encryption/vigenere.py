ALPHABET = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"]

def encrypt(text, key, charset = ALPHABET):
    newText = ""
    
    for i in range(len(text)):
        # Get the index of the current text char in the charset list
        keyChar = key[i % len(key)]
        keyCharIndex = charset.index(keyChar)
        
        # Get the index of the current text char in the charset list
        textChar = text[i]
        textCharIndex = charset.index(textChar)
        
        # Get the index of the new char in the charset list
        newCharIndex = (keyCharIndex + textCharIndex) % len(charset)
        newChar = charset[newCharIndex]
        
        # Append the new char to the new text
        newText += newChar
    
    return newText

def decrypt(text, key, charset = ALPHABET):
    newText = ""
    
    for i in range(len(text)):
        # Get the index of the current text char in the charset list
        keyChar = key[i % len(key)]
        keyCharIndex = charset.index(keyChar)
        
        # Get the index of the current text char in the charset list
        textChar = text[i]
        textCharIndex = charset.index(textChar)
        
        # Get the index of the new char in the charset list
        newCharIndex = (textCharIndex - keyCharIndex) % len(charset)
        newChar = charset[newCharIndex]
        
        # Append the new char to the new text
        newText += newChar
    
    return newText