GET_ALIVE =  [2]
STAY_ALIVE = [2, 3]

def neighboursList(board, x, y, diagonal = True, asCoordinates = False): # Only works with 2D squares
    # Check which cells exists
    left = True if x - 1 >= 0 else False
    right = True if x + 1 <= len(board[y]) - 1 else False
    top = True if y - 1 >= 0 else False
    bottom = True if y + 1 <= len(board) -1 else False
    
    # Create list of existing neighbours
    n = []
    if left:
        n.append([x - 1, y])
    if right:
        n.append([x + 1, y])
    if top:
        n.append([x, y - 1])
    if bottom:
        n.append([x, y + 1])
    if diagonal:
        if left and top:
            n.append([x - 1, y - 1])
        if right and top:
            n.append([x + 1, y - 1])
        if left and bottom:
            n.append([x - 1, y + 1])
        if right and bottom:
            n.append([x + 1, y + 1])
    
    # Convert the coordinates to the corresponding values of the source list
    if not asCoordinates:
        for i, j in enumerate(n):
            n[i] = lst[j[1]][j[0]]
    
    return n

def nextState(currentState, neighbours, getAlive = GET_ALIVE, stayAlive = STAY_ALIVE):
    # Determine the number of neighbours alive
    alive = 0
    for i in neighbours:
        if i == True:
            alive += 1
    
    # Determine and return the next state depending on the number of neighbours alive
    if (alive in getAlive and currentState == False) or (alive in stayAlive and currentState == True):
        return True
    else:
        return False

def duplicateNestedList(lst):
    new = lst[:]
    for n, i in enumerate(new):
        if type(i) == list:
            new[n] = duplicateNestedList(lst[n][:])
    return new

def convertBoard(board, inputFormat, outputFormat, mustMatch = True):
    for i in range(len(board)):
        for j in range(len(board[i])):
            for l in range(len(inputFormat)):
                if board[i][j] == inputFormat[l]:
                    board[i][j] = outputFormat[l]
                    break
            else:
                if mustMatch:
                    raise ValueError

def play(board, format = [True, False], rules = [GET_ALIVE, STAY_ALIVE]):
    convertBoard(board, format, [True, False])
    original = duplicateNestedList(board)
    for y in range(len(board)):
        for x in range(len(board[y])):
            neighbours = neighboursList(original, x, y)
            board[y][x] = nextState(original[y][x], neighbours, getAlive = rules[0], stayAlive = rules[1])
    convertBoard(board, [True, False], format)
    return board